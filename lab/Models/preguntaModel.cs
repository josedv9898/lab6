﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class preguntaModel
    {
        public string enunciado { get; set; }
        public double respuesta { get; set; }
        public preguntaModel(string enunciado, double respuesta)
        {
            this.enunciado = enunciado;
            this.respuesta = respuesta;
        }
    }

}