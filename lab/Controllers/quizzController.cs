﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
namespace WebApplication1.Controllers
{
    public class quizzController : Controller
    {
        // GET: quizz
        public ActionResult pruebaCorta()
        {
            int i = 0;
            preguntaModel[] preguntas = new preguntaModel[20];
            preguntas[0] = new preguntaModel("3x4x5", 60);
            preguntas[1] = new preguntaModel("1+2", 3);
            preguntas[2] = new preguntaModel("10/5", 2);
            preguntas[3] = new preguntaModel("10-9", 1);
            preguntas[4] = new preguntaModel("2x3", 6);
            preguntas[5] = new preguntaModel("15x1", 15);
            preguntas[6] = new preguntaModel("15x3", 45);
            preguntas[7] = new preguntaModel("45x3", 135);
            preguntas[8] = new preguntaModel("1+15", 16);
            preguntas[9] = new preguntaModel("2+8", 10);
            preguntas[10] = new preguntaModel("6-3", 3);
            preguntas[11] = new preguntaModel("3x3", 9);
            preguntas[12] = new preguntaModel("60-50", 10);
            preguntas[13] = new preguntaModel("20x3", 60);
            preguntas[14] = new preguntaModel("1+9", 10);
            preguntas[15] = new preguntaModel("3x3x3", 27);
            preguntas[16] = new preguntaModel("3x4x6", 72);
            preguntas[17] = new preguntaModel("5!", 120);
            preguntas[18] = new preguntaModel("1+2+3", 6);
            preguntas[19] = new preguntaModel("15x2", 30);
            @ViewBag.preguntas = preguntas;
            return View();
        }
    }
}